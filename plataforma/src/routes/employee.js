const express = require('express');
const router = express.Router();

const employeeController = require('../Controllers/employee.controller');

// Ruta para obtener todos los empleados
router.get('/', employeeController.getAllEmployees);

// Ruta para agregar un nuevo empleado
router.post('/', employeeController.addEmployee);

// Ruta para obtener un empleado por nombre o documento
router.get('/:searchValue', employeeController.getEmployee);

// Ruta para editar un empleado existente
router.put('/:id', employeeController.updateEmployee);

module.exports = router;
const Employee = require('../models/employee.model');
const DocumentType = require('../models/document_type.model');
const Area = require ('../models/area.model')
const SubArea = require('../models/sub_area.model')
// Obtener todos los empleados
const getAllEmployeesData = async () => {
  try {
    const documentTypeList = await DocumentType.getDocumentTypes();
    const areaList = await Area.getAreaList();
    const subAreaList = await SubArea.getSubAreaList();
    const employees = await Employee.getAllEmployees();

    return { employees, documentTypeList, areaList, subAreaList};
  } catch (error) {
    console.log(error)
    throw new Error('Error al obtener los empleados');
  }
};









// Agregar un nuevo empleado
const addEmployee = async (employeeData) => {
  try {
    const newEmployee = await Employee.createEmployee(employeeData);
    return newEmployee;
  } catch (error) {
    throw new Error('Error al agregar el empleado');
  }
};

// Obtener un empleado por nombre o documento
const getEmployee = async (searchValue) => {
  try {
    const employee = await Employee.getEmployee(searchValue);
    return employee;
  } catch (error) {
    throw new Error('Error al obtener el empleado');
  }
};

// Editar un empleado existente
const updateEmployee = async (employeeId, employeeData) => {
  try {
    const updatedEmployee = await Employee.updateEmployee(employeeId, employeeData);
    return updatedEmployee;
  } catch (error) {
    throw new Error('Error al editar el empleado');
  }
};

// obtener la lista de tipos de documentos
const getDocumentTypes = async () => {
  DocumentType.getDocumentTypes()
  
}

module.exports = {
  getAllEmployeesData,
  addEmployee,
  getEmployee,
  updateEmployee,
};

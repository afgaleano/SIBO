const { DataTypes, Model } = require('sequelize');
const sequelize = require('../database');

class DocumentType extends Model {}

DocumentType.init(
  {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
  },
  {
    sequelize,
    modelName: 'DocumentType',
    timestamps: false,
  }
);

DocumentType.getDocumentTypes = async () => {
  try {
    const documentTypes = await DocumentType.findAll({
      attributes: ['id', 'name'],
    });
    return documentTypes;
  } catch (error) {
    throw new Error('Error al obtener los tipos de documento');
  }
};

module.exports = DocumentType;
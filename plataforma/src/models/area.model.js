const { DataTypes, Model } = require('sequelize');
const sequelize = require('../database');

//Tabla Area
class Area extends Model {}

Area.init({
  // Model attributes are defined here
  id: {
    type: DataTypes.INTEGER,
    allowNull: false,
    primaryKey: true,
    autoIncrement: true,
  },
  name: {
    type: DataTypes.STRING,
    allowNull: false,
  },
}, {
  timestamps: false, // Deshabilitar los timestamps
  sequelize, // Conexion con la instancia
  modelName: 'Area' // nombre del modelo
});

Area.getAreaList = async () => {
  try {
    const areaList = await Area.findAll({
      attributes: ['id', 'name'],
    });
    return areaList;
  } catch (error) {
    throw new Error('Error al obtener la lista de las areas');
  }
};

module.exports = Area;
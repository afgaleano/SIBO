const { DataTypes, Model } = require('sequelize');
const sequelize = require('../database');

// Tabla SubArea
class SubArea extends Model {}

SubArea.init({
  // Model attributes are defined here
  id: {
    type: DataTypes.INTEGER,
    allowNull: false,
    primaryKey: true,
    autoIncrement: true,
  },
  area: {
    type: DataTypes.INTEGER,
    allowNull: false,
  },
  name: {
    type: DataTypes.STRING,
    allowNull: false,
  },
}, {
  timestamps: false, // Deshabilitar los timestamps
  sequelize, // Conexion con la instancia
  modelName: 'SubArea' // nombre del modelo
});

SubArea.getSubAreaList = async () => {
  try {
    const subAreaList = await SubArea.findAll({
      attributes: ['id', 'area', 'name'],
    });
    return subAreaList;
  } catch (error) {
    throw new Error('Error encontrado lista de sub areas');
  }
};

module.exports = SubArea;
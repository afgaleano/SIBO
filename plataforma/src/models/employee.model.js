const { Sequelize, Model, DataTypes } = require('sequelize');
const { Op } = require("sequelize");

const sequelize = require('../database');

// Tabla EMployee
class Employee extends Model {}

Employee.init({
  // Model attributes are defined here
  document: {
    type: DataTypes.INTEGER,
    allowNull: false,
    primaryKey: true,
    unique: true,
  },
  document_type: {
    type: DataTypes.INTEGER,
    allowNull: false,
  },
  names: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  last_names: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  area: {
    type: DataTypes.INTEGER,
    allowNull: false,
  },
  sub_area: {
    type: DataTypes.INTEGER,
    allowNull: false,
  },
}, {
  timestamps: false, // Deshabilitar los timestamps
  sequelize, // Conexion con la instancia
  modelName: 'Employee' // nombre del modelo
});

//metodos para realizar operaciones en la tabla Employee

// Método para obtener todos los empleados
Employee.getAllEmployees = async () => {
  try {
    const employees = await Employee.findAll({
      attributes: ['document', 'document_type', 'names', 'last_names', 'area', 'sub_area'],
    });
    return employees;
  } catch (error) {
    throw new Error('Error al obtener los empleados');
  }
};

// Método para crear un nuevo empleado
Employee.createEmployee = async (employeeData) => {
  try {
    const employee = await Employee.create(employeeData);
    return employee;
  } catch (error) {
    throw new Error('Error al crear el empleado');
  }
};

// Método para buscar un empleado por nombre o documento
Employee.getEmployee = async (searchValue) => {
  try {
    const employee = await Employee.findOne({
      where: {
        [Op.or]: [
          { names: searchValue },
          { last_names: searchValue },
          { document: searchValue },
        ],
      },
    });
    return employee;
  } catch (error) {
    throw new Error('Error al obtener el empleado');
  }
};

// Método para editar un empleado existente
Employee.updateEmployee = async (employeeId, employeeData) => {
  try {
    const employee = await Employee.findByPk(employeeId);
    if (!employee) {
      throw new Error('Empleado no encontrado');
    }

    await employee.update(employeeData);
    return employee;
  } catch (error) {
    throw new Error('Error al editar el empleado');
  }
};

module.exports = Employee;
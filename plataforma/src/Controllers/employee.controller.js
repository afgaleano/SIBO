const Employee = require('../models/employee.model');
const { getAllEmployeesData } = require('../services/employee.service')
// Obtener todos los empleados
const getAllEmployees = async (req, res) => {
  // validaciones de acceso a la informacion
  /*
  if (!req.user || !req.user.isAdmin) {
    return res.status(403).json({ error: 'Acceso denegado' });
  }
  */
  try{
    const response = await getAllEmployeesData();
    res.status(200).json(response);
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
};

// Agregar un nuevo empleado
const addEmployee = async (req, res) => {
  try {
    const employeeData = req.body;
    const newEmployee = await Employee.createEmployee(employeeData);
    res.status(201).json(newEmployee);
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
};

// Obtener un empleado por nombre o documento
const getEmployee = async (req, res) => {
  // validaciones de acceso a la informacion
  /*
  if (!req.user || !req.user.isAdmin) {
    return res.status(403).json({ error: 'Acceso denegado' });
  }
  */
  try {
    const { searchValue } = req.params;
    const employee = await Employee.getEmployee(searchValue);
    if (employee) {
      // Aquí puedes realizar las validaciones de permisos si es necesario
      res.status(200).json(employee);
    } else {
      res.status(404).json({ message: 'Empleado no encontrado' });
    }
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
};


// Editar un empleado existente
const updateEmployee = async (req, res) => {

  // validaciones de acceso a la informacion
  /*
  if (!req.user || !req.user.isAdmin) {
    return res.status(403).json({ error: 'Acceso denegado' });
  }
  */
  try {
    const { id } = req.params;
    const employeeData = req.body;

    const updatedEmployee = await Employee.updateEmployee(id, employeeData);
    if (updatedEmployee) {
      res.status(200).json(updatedEmployee);
    } else {
      res.status(404).json({ message: 'Empleado no encontrado' });
    }
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
};

module.exports = {
  getAllEmployees,
  addEmployee,
  getEmployee,
  updateEmployee,
};

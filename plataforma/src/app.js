const express = require('express');
const helmet = require('helmet');
const bodyParser = require('body-parser');
const cors = require('cors');
require('dotenv').config();

const employeeRouter = require('./routes/employee');
const sequelize = require('./database');

const app = express();

// Middlewares
app.use(cors());
app.use(helmet());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.json());

// Sincronizar los modelos con la base de datos en entornos de desarrollo
if (process.env.NODE_ENV === 'dev') {
  sequelize.sync({ force: true })
    .then(() => {
      console.log('Tablas creadas en la base de datos');
      startServer();
    })
    .catch((error) => {
      console.error('Error al crear las tablas en la base de datos:', error);
    });
} else {
  startServer();
}

function startServer() {
  // Routes
  app.use('/employees', employeeRouter);

  const port = process.env.PORT || 3000;
  app.listen(port, () => {
    console.log(`Servidor en ejecución en el puerto ${port}.`);
  });
}